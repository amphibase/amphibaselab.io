---
title: "Hyla arborea"
subtitle: "Common Tree Frog"
dirName: "Hyla_arborea"

order: Anura
family: Hylidae

version: "2021_07"
seqs: 15,709
busco_pct: 43.2

amphibiaweb_id: 718 
tax_id: 121869

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,132
* Median Tx length(bp): 857
* Tx longer than 400bp: 12,273 (78.127)
* Tx longer than 1kbp: 6,750 (42.969)
* Tx longer than 5kbp: 131
* Tx longer than 10kbp: 7

###  Peptides
* Mean peptide length(aa):
* Mean pep length(aa): 290
* Median pep length(aa): 218
* Peptides longer than 100aa: 12,943 (82.392)
* Peptides longer than 500aa: 2,225 (14.164)
* Peptides longer than 1000aa: 348
* Peptides longer than 3000aa: 2

### BUSCO
* Vertebrates: C:43.2%[S:41.1%,D:2.1%],F:22.7%,M:34.1%,n:3354
  * 1449    Complete BUSCOs (C)
  * 1377    Complete and single-copy BUSCOs (S)
  * 72      Complete and duplicated BUSCOs (D)
  * 762     Fragmented BUSCOs (F)
  * 1143    Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
