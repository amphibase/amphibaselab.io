---
title: "Hyla cinerea"
subtitle: "North American Green Treefrog"
dirName: Hyla_cinerea

order: Anura
family: Hylidae

version: "2021_07"
seqs: 25,257
busco_pct: 83.7

amphibiaweb_id: 769 
tax_id: 8422

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,697
* Median Tx length(bp): 1,123
* Tx longer than 400bp: 20,295 (80.354)
* Tx longer than 1kbp: 13,510 (53.490)
* Tx longer than 5kbp: 1,270
* Tx longer than 10kbp: 134

###  Peptides
* Mean peptide length(aa):
* Mean pep length(aa): 390
* Median pep length(aa): 246
* Peptides longer than 100aa: 20,652 (81.767)
* Peptides longer than 500aa: 6,086 (24.096)
* Peptides longer than 1000aa: 1,801
* Peptides longer than 3000aa: 96

### BUSCO
* Vertebrates: C:83.7%[S:80.1%,D:3.6%],F:7.0%,M:9.3%,n:3354
  * 2809    Complete BUSCOs (C)
  * 2688    Complete and single-copy BUSCOs (S)
  * 121     Complete and duplicated BUSCOs (D)
  * 236     Fragmented BUSCOs (F)
  * 309     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
