---
title: Hyla suweonensis
subtitle: Suweon treefrog 

order: Anura
family: Hylidae

version: "2021_07"
seqs: 22,929
busco_pct: 93.2
amphibiaweb_id: 971
tax_id: 1926317
dirName: Hyla_suweonensis

layout: species
---

### Transcripts 
* Mean tx length(bp): 1,926
* Median tx length(bp): 1,530
* Tx longer than 400bp: 20,969 (91.452)
* Tx longer than 1kbp: 15,381 (67.081)
* Tx longer than 5kbp: 1,096
* Tx longer than 10kbp: 65

###  Peptides
* Mean peptide length(aa): 452
* Median peptide length(aa): 336:
* Peptides longer than 100aa: 21,760 (94.902)
* Peptides longer than 500aa: 7,057 (30.778)
* Peptides longer than 1000aa: 1,881
* Peptides longer than 3000aa: 51

### BUSCO
* Vertebrates: C:93.2%[S:89.0%,D:4.2%],F:2.5%,M:4.3%,n:3354 
  * 3127   Complete BUSCOs (C)
  * 2985   Complete and single-copy BUSCOs (S)
  * 142    Complete and duplicated BUSCOs (D)
  * 83     Fragmented BUSCOs (F)
  * 144    Missing BUSCOs (M)
  * 3354   Total BUSCO groups searched
