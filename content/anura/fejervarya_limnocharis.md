---
title: "Fejervarya_limnocharis"
subtitle: "Alpine Cricket Frog"
dirName: "Fejervarya_limnocharis"

order: Anura
family: Dicroglossidae

version: "2021_07"
seqs: 18,088
busco_pct: 70.9

amphibiaweb_id: 4770 
tax_id: 110108

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,750
* Median Tx length(bp): 1,350
* Tx longer than 400bp: 16,358 (90.436)
* Tx longer than 1kbp: 11,144 (61.610)
* Tx longer than 5kbp: 734
* Tx longer than 10kbp: 20

###  Peptides
* Mean pep length(aa): 390
* Median pep length(aa): 292
* Peptides longer than 100aa: 16,703 (92.343)
* Peptides longer than 500aa: 4,509 (24.928)
* Peptides longer than 1000aa: 1,036
* Peptides longer than 3000aa: 12

### BUSCO
* Vertebrates: C:70.9%[S:67.4%,D:3.5%],F:12.1%,M:17.0%,n:3354
  * 2376    Complete BUSCOs (C)
  * 2259    Complete and single-copy BUSCOs (S)
  * 117     Complete and duplicated BUSCOs (D)
  * 407     Fragmented BUSCOs (F)
  * 571     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
