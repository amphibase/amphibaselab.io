---
title: Anura species at AmphiBase
date: 2021-07-19
layout: order_list

AvailableFamilies: [
    "Bombinatoridae (10)", 
    "Bufonidae (637)",
    "Dendrobatidae (329)", 
    "Dicroglossidae (219)", 
    "Hylidae (1022)", 
    "Leptodactylidae (224)", 
    "Mantellidae (232)", 
    "Megophryidae (273)", 
    "Microhylidae (696)", 
    "Pelobatidae (6)", 
    "Pipidae (41)", 
    "Pyxicephalidae (87)", 
    "Ranidae (428)", 
    "Rhacophoridae (439)", 
    "Scaphiopodidae (7)", 
    "Strabomantidae (745)"
]

NotAvailableFamilies: [
  "Allophrynidae (3)",
  "Alsodidae (26)",
  "Alytidae (12)",
  "Arthroleptidae (151)",
  "Ascaphidae (2)",
  "Batrachylidae (13)",
  "Brachycephalidae (74)",
  "Brevicipitidae (36)",
  "Calyptocephalellidae (5)",
  "Centrolenidae (160)",
  "Ceratobatrachidae (102)",
  "Ceratophryidae (12)",
  "Ceuthomantidae (4)",
  "Conrauidae (6)",
  "Craugastoridae (123)",
  "Cycloramphidae (36)",
  "Eleutherodactylidae (229)",
  "Heleophrynidae (6)",
  "Hemiphractidae (118)",
  "Hemisotidae (9)",
  "Hylodidae (47)",
  "Hyperoliidae (232)",
  "Leiopelmatidae (3)",
  "Micrixalidae (24)",
  "Myobatrachidae (133)",
  "Nasikabatrachidae (2)",
  "Nyctibatrachidae (39)",
  "Odontobatrachidae (5)",
  "Odontophrynidae (50)",
  "Pelodytidae (4)",
  "Petropedetidae (13)",
  "Phrynobatrachidae (97)",
  "Ptychadenidae (63)",
  "Ranixalidae (19)",
  "Rhinodermatidae (3)",
  "Rhinophrynidae (1)",
  "Sooglossidae (4)",
  "Telmatobiidae (63)",
]
---

**Repository:** https://pub.amphibase.org/draft/Anura/
