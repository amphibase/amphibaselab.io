---
title: "Pelophylax nigromaculatus"
subtitle: "Dark-Spotted Frog" 
dirName: Pelophylax_nigromaculatus

order: Anura
family: Ranidae

version: "2021_07"
seqs: 19,273
busco_pct: 70.5

amphibiaweb_id: 5109 
tax_id: 8409

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,517
* Median Tx length(bp): 1,112
* Tx longer than 400bp: 15,315 (79.463)
* Tx longer than 1kbp: 10,317 (53.531)
* Tx longer than 5kbp: 639
* Tx longer than 10kbp: 31

###  Peptides
* Mean pep length(aa): 361
* Median pep length(aa): 257
* Peptides longer than 100aa: 15,813 (82.047)
* Peptides longer than 500aa: 4,309 (22.358)
* Peptides longer than 1000aa: 995
* Peptides longer than 3000aa: 21

### BUSCO
* Vertebrates: C:70.5%[S:67.3%,D:3.2%],F:9.7%,M:19.8%,n:3354
  * 2366    Complete BUSCOs (C)
  * 2258    Complete and single-copy BUSCOs (S)
  * 108     Complete and duplicated BUSCOs (D)
  * 324     Fragmented BUSCOs (F)
  * 664     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
