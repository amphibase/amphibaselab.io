---
title: "Hoplobatrachus rugulosus"
subtitle: "Asian Rugose Gullfrog"
dirName: "Hoplobatrachus_rugulosus" 

order: Anura
family: Dicroglossidae

version: "2021_07"
seqs: 17,722
busco_pct: 82.9

amphibiaweb_id: 
tax_id: 110072

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,443
* Median Tx length(bp): 1,899
* Tx longer than 400bp: 16,829 (94.961)
* Tx longer than 1kbp: 13,233 (74.670)
* Tx longer than 5kbp: 1,885
* Tx longer than 10kbp: 65


###  Peptides
* Mean pep length(aa): 453
* Median pep length(aa): 355
* Peptides longer than 100aa: 16,823 (94.927)
* Peptides longer than 500aa: 5,693 (32.124)
* Peptides longer than 1000aa: 1,382
* Peptides longer than 3000aa: 5


### BUSCO
* Vertebrates: C:82.9%[S:79.6%,D:3.3%],F:7.1%,M:10.0%,n:3354
  * 2780    Complete BUSCOs (C)
  * 2670    Complete and single-copy BUSCOs (S)
  * 110     Complete and duplicated BUSCOs (D)
  * 238     Fragmented BUSCOs (F)
  * 336     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
