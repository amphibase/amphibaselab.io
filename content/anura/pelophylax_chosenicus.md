---
title: "Pelophylax chosenicus"
subtitle: "Seoul Frog"
dirName: Pelophylax_chosenicus 

order: Anura
family: Ranidae

version: "2021_07"
seqs: 22,454
busco_pct: 84.9

amphibiaweb_id:  
tax_id: 512019

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,796
* Median Tx length(bp): 1,318
* Tx longer than 400bp: 18,413 (82.003)
* Tx longer than 1kbp: 13,387 (59.620)
* Tx longer than 5kbp: 1,195
* Tx longer than 10kbp: 81

###  Peptides
* Mean peptide length(aa): 395
* Median peptide length(aa): 278
* Peptides longer than 100aa: 18,813 (83.785)
* Peptides longer than 500aa: 5,785 (25.764)
* Peptides longer than 1000aa: 1,512
* Peptides longer than 3000aa: 35

### BUSCO
* Vertebrates: C:84.9%[S:81.0%,D:3.9%],F:5.9%,M:9.2%,n:3354
  * 2848    Complete BUSCOs (C)
  * 2716    Complete and single-copy BUSCOs (S)
  * 132     Complete and duplicated BUSCOs (D)
  * 199     Fragmented BUSCOs (F)
  * 307     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
