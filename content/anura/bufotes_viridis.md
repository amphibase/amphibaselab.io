---
title: "Bufotes viridis" 
subtitle: "Green Toad"
dirName: "Bufotes_viridis"

order: Anura
family: Bufonidae

version: "2021_07"
seqs: 14,617
busco_pct: 53.5

amphibiaweb_id: 312 
tax_id: 30338

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,411
* Median Tx length(bp): 1,108
* Tx longer than 400bp: 13,145 (89.930)
* Tx longer than 1kbp: 8,003 (54.751)
* Tx longer than 5kbp: 169
* Tx longer than 10kbp: 2

###  Peptides
* Mean pep length(aa): 340
* Median pep length(aa): 264
* Peptides longer than 100aa: 13,832 (94.630)
* Peptides longer than 500aa: 2,770 (18.951)
* Peptides longer than 1000aa: 438
* Peptides longer than 3000aa: 1

### BUSCO
* Vertebrates: C:53.5%[S:52.7%,D:0.8%],F:23.6%,M:22.9%,n:3354
  * 1796    Complete BUSCOs (C)
  * 1769    Complete and single-copy BUSCOs (S)
  * 27      Complete and duplicated BUSCOs (D)
  * 792     Fragmented BUSCOs (F)
  * 766     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
