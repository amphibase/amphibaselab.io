---
title: Hymenochirus_boettgeri
subtitle: Zaire Dwarf Clawed Frog
dirName: Hymenochirus_boettgeri

order: Anura
family: Pipidae

version: "2021_07"
seqs: 18,782
busco_pct: 65.6

amphibiaweb_id: 5238 
tax_id: 247094

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,368
* Median Tx length(bp): 1,034
* Tx longer than 400bp: 14,987 (79.794)
* Tx longer than 1kbp: 9,656 (51.411)
* Tx longer than 5kbp: 347
* Tx longer than 10kbp: 12


###  Peptides
* Mean peptide length(aa): 313
* Median peptide length(aa): 244
* Peptides longer than 100aa: 15,630 (83.218)
* Peptides longer than 500aa: 3,130 (16.665)
* Peptides longer than 1000aa: 472
* Peptides longer than 3000aa: 3


### BUSCO
* Vertebrates: C:65.6%[S:62.7%,D:2.9%],F:17.7%,M:16.7%,n:3354
  * 2200        Complete BUSCOs (C)
  * 2102        Complete and single-copy BUSCOs (S)
  * 98  Complete and duplicated BUSCOs (D)
  * 595 Fragmented BUSCOs (F)
  * 559 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

