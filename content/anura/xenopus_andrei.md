---
title: Xenopus andrei
subtitle: Andre's Clawed Frog 
dirName: Xenopus_andrei

order: Anura
family: Pipidae

version: "2021_07"
seqs: 19,345
busco_pct: 88.6

amphibiaweb_id: 
tax_id: 288519

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,959
* Median Tx length(bp): 1,609
* Tx longer than 400bp: 19,271 (99.617)
* Tx longer than 1kbp: 13,743 (71.042)
* Tx longer than 5kbp: 782
* Tx longer than 10kbp: 43


###  Peptides
* Mean peptide length(aa): 469
* Median peptide length(aa): 355
* Peptides longer than 100aa: 18,618 (96.242)
* Peptides longer than 500aa: 6,343 (32.789)
* Peptides longer than 1000aa: 1,594
* Peptides longer than 3000aa: 43


### BUSCO
* Vertebrates: C:88.6%[S:84.4%,D:4.2%],F:4.9%,M:6.5%,n:3354
  * 2972        Complete BUSCOs (C)
  * 2831        Complete and single-copy BUSCOs (S)
  * 141 Complete and duplicated BUSCOs (D)
  * 163 Fragmented BUSCOs (F)
  * 219 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

