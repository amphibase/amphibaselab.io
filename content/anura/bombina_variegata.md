---
title: "Bombina variegata"
subtitle: "Yellow-Bellied Toad"
dirName: "Bombina_variegata"

order: Anura
family: Bombinatoridae

version: "2021_07"
seqs: 16,361
busco_pct: 81.1

amphibiaweb_id: 2046
tax_id: 8348

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,092
* Median Tx length(bp): 1,689
* Tx longer than 400bp: 15,122 (92.484)
* Tx longer than 1kbp: 11,612 (71.017)
* Tx longer than 5kbp: 985
* Tx longer than 10kbp: 67

###  Peptides
* Mean pep length(aa): 456
* Median pep length(aa): 341
* Peptides longer than 100aa: 15,552 (95.113)
* Peptides longer than 500aa: 5,094 (31.154)
* Peptides longer than 1000aa: 1,348
* Peptides longer than 3000aa: 49

### BUSCO
* Vertebrates: C:81.1%[S:77.8%,D:3.3%],F:7.3%,M:11.6%,n:3354
  * 2722    Complete BUSCOs (C)
  * 2611    Complete and single-copy BUSCOs (S)
  * 111     Complete and duplicated BUSCOs (D)
  * 245     Fragmented BUSCOs (F)
  * 387     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
