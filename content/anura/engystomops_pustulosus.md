---
title: "Engystomops pustulosus" 
subtitle: "Túngara frog"
dirName: "Engystomops_pustulosus"

order: Anura
family:  Leptodactylidae

version: "2021_07"
seqs: 23,490
busco_pct: 79.9

amphibiaweb_id: 3414
tax_id: 76066

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,787
* Median Tx length(bp): 1,277
* Tx longer than 400bp: 19,689 (83.819)
* Tx longer than 1kbp: 13,656 (58.135)
* Tx longer than 5kbp: 1,232
* Tx longer than 10kbp: 107


###  Peptides
* Mean pep length(aa): 414
* Median pep length(aa): 286
* Peptides longer than 100aa: 20,239 (86.160)
* Peptides longer than 500aa: 6,244 (26.582)
* Peptides longer than 1000aa: 1719
* Peptides longer than 3000aa: 81

### BUSCO
* Vertebrates: C:79.9%[S:76.2%,D:3.7%],F:9.4%,M:10.7%,n:3354
  * 2681    Complete BUSCOs (C)
  * 2556    Complete and single-copy BUSCOs (S)
  * 125     Complete and duplicated BUSCOs (D)
  * 315     Fragmented BUSCOs (F)
  * 358     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
