---
title: "Pelophylax lessonae"
subtitle: "Pool Frog" 
dirName: Pelophylax_lessonae

order: Anura
family: Ranidae

version: "2021_07"
seqs: 20,322
busco_pct: 53.4

amphibiaweb_id: 5077 
tax_id: 45623

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,081
* Median Tx length(bp): 703
* Tx longer than 400bp: 13,717 (67.498)
* Tx longer than 1kbp: 7,814 (38.451)
* Tx longer than 5kbp: 264
* Tx longer than 10kbp: 12


###  Peptides
* Number of peptides: 20322 20322
* Mean peptide length(aa): 279
* Median peptide length(aa): 186
* Peptides longer than 100aa: 14,955 (73.590)
* Peptides longer than 500aa: 2,830 (13.926)
* Peptides longer than 1000aa: 530
* Peptides longer than 3000aa: 9


### BUSCO
* Vertebrates: C:53.4%[S:51.3%,D:2.1%],F:21.6%,M:25.0%,n:3354
  * 1791    Complete BUSCOs (C)
  * 1719    Complete and single-copy BUSCOs (S)
  * 72      Complete and duplicated BUSCOs (D)
  * 724     Fragmented BUSCOs (F)
  * 839     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
