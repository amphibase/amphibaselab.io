---
title: Rana sylvatica
subtitle: Wood frog
dirName: Rana_sylvatica

order: Anura
family: Ranidae

version: "2021_07"
seqs: 22,685
busco_pct: 69.0

amphibiaweb_id: 5162
tax_id: 45438

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,533
* Median Tx length(bp): 998
* Tx longer than 400bp: 17,707 (78.056)
* Tx longer than 1kbp: 11,326 (49.927)
* Tx longer than 5kbp: 888
* Tx longer than 10kbp: 39


###  Peptides
* Mean peptide length(aa): 299
* Median peptide length(aa): 189
* Peptides longer than 100aa: 16,014 (70.593)
* Peptides longer than 500aa: 3,957 (17.443)
* Peptides longer than 1000aa: 894
* Peptides longer than 3000aa: 12


### BUSCO
* Vertebrates: C:69.0%[S:65.9%,D:3.1%],F:13.1%,M:17.9%,n:3354
  * 2314        Complete BUSCOs (C)
  * 2209        Complete and single-copy BUSCOs (S)
  * 105 Complete and duplicated BUSCOs (D)
  * 438 Fragmented BUSCOs (F)
  * 602 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

