---
title: Oophaga_pumilio
subtitle: "Strawberry Poison Frog"
dirName: Oophaga_pumilio 

order: Anura
family: Dendrobatidae

version: "2021_07"
seqs: 18,818
busco_pct: 59.8

amphibiaweb_id: 1641 
tax_id: 51950

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,536
* Median Tx length(bp): 1,144
* Tx longer than 400bp: 15,119 (80.343)
* Tx longer than 1kbp: 10,348 (54.990)
* Tx longer than 5kbp: 556
* Tx longer than 10kbp: 27

###  Peptides
* Mean peptide length(aa): 327
* Median peptide length(aa): 240
* Peptides longer than 100aa: 15,290 (81.252)
* Peptides longer than 500aa: 3,442 (18.291)
* Peptides longer than 1000aa: 656
* Peptides longer than 3000aa: 17

### BUSCO
* Vertebrates: C:59.8%[S:58.7%,D:1.1%],F:21.4%,M:18.8%,n:3354
  * 2008    Complete BUSCOs (C)
  * 1970    Complete and single-copy BUSCOs (S)
  * 38      Complete and duplicated BUSCOs (D)
  * 717     Fragmented BUSCOs (F)
  * 629     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched

  *
