---
title: "Odorrana margaretae"
subtitle: "Green Odorous Frog"
dirName: Odorrana_margaretae

order: Anura
family: Ranidae

version: "2021_07"
seqs: 15,023
busco_pct: 71.1

amphibiaweb_id: 5092 
tax_id: 121156

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,904
* Median Tx length(bp): 1,594
* Tx longer than 400bp: 13,781 (91.733)
* Tx longer than 1kbp: 10,281 (68.435)
* Tx longer than 5kbp: 521
* Tx longer than 10kbp: 6


###  Peptides
* Mean pep length(aa): 384
* Median pep length(aa): 308
* Peptides longer than 100aa: 14,336 (95.427)
* Peptides longer than 500aa: 3,787 (25.208)
* Peptides longer than 1000aa: 630
* Peptides longer than 3000aa: 2


### BUSCO
* Vertebrates: C:71.1%[S:69.7%,D:1.4%],F:11.9%,M:17.0%,n:3354
  * 2385    Complete BUSCOs (C)
  * 2339    Complete and single-copy BUSCOs (S)
  * 46      Complete and duplicated BUSCOs (D)
  * 400     Fragmented BUSCOs (F)
  * 569     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
