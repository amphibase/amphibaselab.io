---
title: "Kaloula borealis"
subtitle: "Boreal Digging Frog"

order: "Anura"
family: "Microhylidae"

version: "2021_07"
seqs: 18,160
busco_pct: 80.8
amphibiaweb_id: 2152
tax_id: 113360

layout: species
---

### Transcripts 
* Mean length(bp): 1,839
* Median length(bp): 1,457
* Tx longer than 400bp: 15,784 (86.916)
* Tx longer than 1kbp: 11,695 (64.400)
* Tx longer than 5kbp: 815
* Tx longer than 10kbp: 33

###  Peptides
* Mean length(aa): 406
* Median length(aa): 310
* Peptides longer than 100aa: 15,907 (87.594)
* Peptides longer than 500aa: 4,953 (27.274)
* Peptides longer than 1000aa: 1,150
* Peptides longer than 3000aa: 12

### BUSCO5 
* Vertebrates: 80.8%[S:77.0%,D:3.8%],F:7.7%,M:11.5%,n:3354
  * 2710    Complete BUSCOs (C)
  * 2584    Complete and single-copy BUSCOs (S)
  * 126     Complete and duplicated BUSCOs (D)
  * 259     Fragmented BUSCOs (F)
  * 385     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
