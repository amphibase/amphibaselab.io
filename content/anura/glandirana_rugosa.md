---
title: "Glandirana rugosa"
subtitle: "Wrinkled Frog"
dirName: "Glandirana_rugosa"

order: Anura
family: "Ranidae"

version: "2021_07"
seqs: 21,414
busco_pct: 89.2

amphibiaweb_id: 5138 
tax_id: 8410

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,041
* Median Tx length(bp): 1,528
* Tx longer than 400bp: 19,288 (90.072)
* Tx longer than 1kbp: 13,992 (65.340)
* Tx longer than 5kbp: 1,515
* Tx longer than 10kbp: 63

###  Peptides
* Mean pep length(aa): 425
* Median pep length(aa): 310
* Peptides longer than 100aa: 20,127 (93.990)
* Peptides longer than 500aa: 6,045 (28.229)
* Peptides longer than 1000aa: 1,603
* Peptides longer than 3000aa: 25

### BUSCO
* Vertebrates: C:89.2%[S:85.2%,D:4.0%],F:4.0%,M:6.8%,n:3354
  * 2992    Complete BUSCOs (C)
  * 2859    Complete and single-copy BUSCOs (S)
  * 133     Complete and duplicated BUSCOs (D)
  * 135     Fragmented BUSCOs (F)
  * 227     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
