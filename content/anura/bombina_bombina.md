---
title: "Bombina bombina"
subtitle: "Fire-Bellied Toad"
dirName: Bombina_bombina

order: Anura
family: Bombinatoridae

version: "2021_07"
seqs: 14,061
busco_pct: 72.4

amphibiaweb_id: 2041
tax_id: 8345

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,206
* Median Tx length(bp): 1,776
* Tx longer than 400bp: 13,074 (92.981)
* Tx longer than 1kbp: 10,143 (72.136)
* Tx longer than 5kbp: 1,026
* Tx longer than 10kbp: 68

###  Peptides
* Mean pep length(aa): 452
* Median pep length(aa): 339
* Peptides longer than 100aa: 13,417 (95.420)
* Peptides longer than 500aa: 4,286 (30.481)
* Peptides longer than 1000aa: 1,114
* Peptides longer than 3000aa: 38

### BUSCO
* Vertebrates:  C:72.4%[S:70.9%,D:1.5%],F:11.7%,M:15.9%,n:3354
  * 2427    Complete BUSCOs (C)
  * 2377    Complete and single-copy BUSCOs (S)
  * 50      Complete and duplicated BUSCOs (D)
  * 394     Fragmented BUSCOs (F)
  * 533     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched

  *
