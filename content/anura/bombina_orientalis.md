---
title: "Bombina orientalis"
subtitle: "Oriental Fire-Bellied Toad"
dirName: "Bombina_orientalis"

order: Anura
family: Bombinatoridae

version: "2021_07"
seqs: 26,136
busco_pct: 85.5

amphibiaweb_id: 2045
tax_id: 8346

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,397
* Median Tx length(bp): 1,045
* Tx longer than 400bp: 20,210 (77.326)
* Tx longer than 1kbp: 13,451 (51.465)
* Tx longer than 5kbp: 527
* Tx longer than 10kbp: 26

###  Peptides
* Mean pep length(aa): 355
* Median pep length(aa): 249
* Peptides longer than 100aa: 21,075 (80.636)
* Peptides longer than 500aa: 5,875 (22.479)
* Peptides longer than 1000aa: 1,357
* Peptides longer than 3000aa: 23

### BUSCO
* Vertebrates: C:85.5%[S:81.8%,D:3.7%],F:6.6%,M:7.9%,n:3354
  * 2868    Complete BUSCOs (C)
  * 2745    Complete and single-copy BUSCOs (S)
  * 123     Complete and duplicated BUSCOs (D)
  * 220     Fragmented BUSCOs (F)
  * 266     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched

  *
