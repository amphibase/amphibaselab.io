---
title: Rana catesbeiana
subtitle: American Bullfrog
dirName: Rana_catesbeiana

order: Anura
family: Ranidae

version: "2021_07"
seqs: 24,078
busco_pct: 74.0

amphibiaweb_id: 4999
tax_id: 8400

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,631
* Median Tx length(bp): 1,060
* Tx longer than 400bp: 19,321 (80.243)
* Tx longer than 1kbp: 12,489 (51.869)
* Tx longer than 5kbp: 1,100
* Tx longer than 10kbp: 58

###  Peptides
* Mean peptide length(aa): 305
* Median peptide length(aa): 193
* Peptides longer than 100aa: 17,075 (70.915)
* Peptides longer than 500aa: 4,344 (18.041)
* Peptides longer than 1000aa: 1,036
* Peptides longer than 3000aa: 20

### BUSCO
* Vertebrates: C:74.0%[S:70.8%,D:3.2%],F:10.6%,M:15.4%,n:3354
  * 2479        Complete BUSCOs (C)
  * 2373        Complete and single-copy BUSCOs (S)
  * 106 Complete and duplicated BUSCOs (D)
  * 357 Fragmented BUSCOs (F)
  * 518 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

