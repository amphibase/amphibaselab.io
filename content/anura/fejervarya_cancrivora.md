---
title: "Fejervarya cancrivora"
subtitle: "Crab-eating Frog"
dirName: Fejervarya_cancrivora

order: Anura
family: Dicroglossidae

version: "2021_07"
seqs: 18,778
busco_pct: 76.9

amphibiaweb_id: 4748
tax_id: 111367

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,001
* Median Tx length(bp): 1,476
* Tx longer than 400bp: 16,853 (89.749)
* Tx longer than 1kbp: 12,200 (64.970)
* Tx longer than 5kbp: 1,345
* Tx longer than 10kbp: 51

###  Peptides
* Mean pep length(aa): 407
* Median pep length(aa): 311
* Peptides longer than 100aa: 17,307 (92.166)
* Peptides longer than 500aa: 5,124 (27.287)
* Peptides longer than 1000aa: 1,174
* Peptides longer than 3000aa: 8

### BUSCO
* Vertebrates: C:76.9%[S:73.7%,D:3.2%],F:10.7%,M:12.4%,n:3354
  * 2580    Complete BUSCOs (C)
  * 2472    Complete and single-copy BUSCOs (S)
  * 108     Complete and duplicated BUSCOs (D)
  * 360     Fragmented BUSCOs (F)
  * 414     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
