---
title: "Rana tagoi"
subtitle: "Tago Frog"
dirName: Rana_tagoi

order: Anura
family: Ranidae

version: "2021_07"
seqs: 16,372
busco_pct: 68.4

amphibiaweb_id: "" 
tax_id: 113384

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,828
* Median Tx length(bp): 1,338
* Tx longer than 400bp: 14,302 (87.356)
* Tx longer than 1kbp: 10,095 (61.660)
* Tx longer than 5kbp: 890
* Tx longer than 10kbp: 66

###  Peptides
* Mean pep length(aa): 399
* Median pep length(aa): 290
* Peptides longer than 100aa: 14,549 (88.865)
* Peptides longer than 500aa: 4,093 (25.000)
* Peptides longer than 1000aa: 972
* Peptides longer than 3000aa: 33

### BUSCO
* Vertebrates:  C:68.4%[S:65.0%,D:3.4%],F:10.7%,M:20.9%,n:3354
  * 2294    Complete BUSCOs (C)
  * 2180    Complete and single-copy BUSCOs (S)
  * 114     Complete and duplicated BUSCOs (D)
  * 358     Fragmented BUSCOs (F)
  * 702     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
