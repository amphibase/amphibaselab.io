---
title: "Ranitomeya imitator"
subtitle: "Imitating Poison Frog"
dirName: Ranitomeya_imitator

order: Anura
family: Dendrobatidae

version: "2021_07"
seqs: 19,628
busco_pct: 83.4

amphibiaweb_id: 1634 
tax_id: 111125

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,320
* Median Tx length(bp): 1,813
* Tx longer than 400bp: 16,808 (85.633)
* Tx longer than 1kbp: 13,736 (69.982)
* Tx longer than 5kbp: 1,918
* Tx longer than 10kbp: 147

###  Peptides
* Mean pep length(aa): 444
* Median pep length(aa): 329
* Peptides longer than 100aa: 16,754 (85.358)
* Peptides longer than 500aa: 5,958 (30.355)
* Peptides longer than 1000aa: 1,637
* Peptides longer than 3000aa: 60


### BUSCO
* Vertebrates: C:83.4%[S:81.9%,D:1.5%],F:7.8%,M:8.8%,n:3354
  * 2798    Complete BUSCOs (C)
  * 2747    Complete and single-copy BUSCOs (S)
  * 51      Complete and duplicated BUSCOs (D)
  * 261     Fragmented BUSCOs (F)
  * 295     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
