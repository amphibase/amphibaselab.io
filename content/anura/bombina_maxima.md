---
title: "Bombina maxima"
subtitle: 
dirName: "Bombina_maxima"

order: Anura
family: Bombinatoridae

version: "2021_07"
seqs: 17,985
busco_pct: 53.1

amphibiaweb_id: 
tax_id: 161274

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,209
* Median Tx length(bp): 868
* Tx longer than 400bp: 13,494 (75.029)
* Tx longer than 1kbp: 8,031 (44.654)
* Tx longer than 5kbp: 249
* Tx longer than 10kbp: 15

###  Peptides
* Mean pep length(aa): 316
* Median pep length(aa): 220
* Peptides longer than 100aa: 14,240 (79.177)
* Peptides longer than 500aa: 3,189 (17.731)
* Peptides longer than 1000aa: 654
* Peptides longer than 3000aa: 13

### BUSCO
* Vertebrates: C:53.1%[S:50.7%,D:2.4%],F:22.8%,M:24.1%,n:3354
  * 1782    Complete BUSCOs (C)
  * 1701    Complete and single-copy BUSCOs (S)
  * 81      Complete and duplicated BUSCOs (D)
  * 765     Fragmented BUSCOs (F)
  * 807     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
