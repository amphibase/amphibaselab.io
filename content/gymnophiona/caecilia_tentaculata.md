---
title: "Caecilia tentaculata"
subtitle: ""
dirName: "Caecilia_tentaculata"

order: "Gymnophiona"
family: "Caeciliidae"

version: "2021_07"
seqs: 22,226
busco_pct: 90.7

amphibiaweb_id: 1872
tax_id: 356198

layout: species
---

### Transcripts 
* Number of transcripts: 22226
* Mean length(bp): 2,428
* Median length(bp): 1,915
* Tx longer than 400bp: 20,449 (92.005)
* Tx longer than 1kbp: 16,054 (72.231)
* Tx longer than 5kbp: 2,339
* Tx longer than 10kbp: 177


###  Peptides
* Mean length(aa): 443
* Median length(aa): 326
* Peptides longer than 100aa: 20,927 (94.155)
* Peptides longer than 500aa: 6,633 (29.843)
* Peptides longer than 1000aa: 1,792
* Peptides longer than 3000aa: 43


### BUSCO5 
* Vertebrates: C:90.7%[S:87.5%,D:3.2%],F:4.5%,M:4.8%,n:3354
  * 3041    Complete BUSCOs (C)
  * 2935    Complete and single-copy BUSCOs (S)
  * 106     Complete and duplicated BUSCOs (D)
  * 151     Fragmented BUSCOs (F)
  * 162     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
