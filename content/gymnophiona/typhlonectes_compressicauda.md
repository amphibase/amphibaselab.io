---
title: "Typhlonectes compressicauda"
subtitle: "Cayenne Caecilian"
dirName: "Typhlonectes_compressicauda"

order: "Gymnophiona"
family: "Typhlonectidae"

version: "2021_07"
seqs: 20,948
busco_pct: 76.0

amphibiaweb_id: 1962 
tax_id: 324354

layout: species
---

### Transcripts 
* Mean tx length(bp): 
* Mean Tx length(bp): 1,857
* Median Tx length(bp): 1,429
* Tx longer than 400bp: 17,669 (84.347)
* Tx longer than 1kbp: 13,177 (62.903)
* Tx longer than 5kbp: 1,102
* Tx longer than 10kbp: 55


###  Peptides
* Mean pep length(aa): 376
* Median pep length(aa): 276
* Peptides longer than 100aa: 17,861 (85.264)
* Peptides longer than 500aa: 5,023 (23.978)
* Peptides longer than 1000aa: 1,156
* Peptides longer than 3000aa: 9


### BUSCO
* Vertebrates: C:76.0%[S:73.1%,D:2.9%],F:9.8%,M:14.2%,n:3354
  * 2548    Complete BUSCOs (C)
  * 2452    Complete and single-copy BUSCOs (S)
  * 96      Complete and duplicated BUSCOs (D)
  * 329     Fragmented BUSCOs (F)
  * 477     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
