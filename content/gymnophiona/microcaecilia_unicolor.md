---
title: "Microcaecilia unicolor"
subtitle: "Tiny Cayenne Caecilian"
dirName: "Microcaecilia_unicolor"

order: "Gymnophiona"
family: "Siphonopidae"

version: "2021_07"
seqs: 22,770
busco_pct: 88.6

amphibiaweb_id: 1913
tax_id: 1415580

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,024
* Median Tx length(bp): 1,590
* Tx longer than 400bp: 19,429 (85.327)
* Tx longer than 1kbp: 14,944 (65.630)
* Tx longer than 5kbp: 1,482
* Tx longer than 10kbp: 102

###  Peptides
* Mean pep length(aa): 410
* Median pep length(aa): 297
* Peptides longer than 100aa: 19,583 (86.004)
* Peptides longer than 500aa: 6,229 (27.356)
* Peptides longer than 1000aa: 1,576
* Peptides longer than 3000aa: 40

### BUSCO
* Vertebrates: C:88.6%[S:85.0%,D:3.6%],F:4.7%,M:6.7%,n:3354
  * 2970    Complete BUSCOs (C)
  * 2850    Complete and single-copy BUSCOs (S)
  * 120     Complete and duplicated BUSCOs (D)
  * 159     Fragmented BUSCOs (F)
  * 225     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
