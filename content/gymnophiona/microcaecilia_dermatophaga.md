---
title: "Microcaecilia dermatophaga"
subtitle: "Angoulême microcaecilia"
dirName: "Microcaecilia_dermatophaga"

order: "Gymnophiona"
family: "Siphonopidae"

version: "2021_07"
seqs: 18,336
busco_pct: 76.1

amphibiaweb_id: 7976 
tax_id: 1415579

layout: species
---

### Transcripts 
* Number of transcripts: 18,336
* Mean Tx length(bp): 2,085
* Median Tx length(bp): 1,653
* Tx longer than 400bp: 15,993 (87.222)
* Tx longer than 1kbp: 12,442 (67.856)
* Tx longer than 5kbp: 1,258
* Tx longer than 10kbp: 81

###  Peptides
* Mean pep length(aa): 403
* Median pep length(aa): 302
* Peptides longer than 100aa: 16,013 (87.331)
* Peptides longer than 500aa: 4,849 (26.445)
* Peptides longer than 1000aa: 1,138
* Peptides longer than 3000aa: 22

### BUSCO
* Vertebrates: C:76.1%[S:73.1%,D:3.0%],F:9.1%,M:14.8%,n:3354
  * 2551    Complete BUSCOs (C)
  * 2452    Complete and single-copy BUSCOs (S)
  * 99      Complete and duplicated BUSCOs (D)
  * 306     Fragmented BUSCOs (F)
  * 497     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
