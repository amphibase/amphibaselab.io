---
title: "Ichthyophis bannanicus"
subtitle: "Banna Caecilian"
dirName: "Ichthyophis_bannanicus"

family: "Ichthyophiidae"
order: "Gymnophiona"

version: "2021_07"
seqs: 20,325
busco_pct: 65.4

amphibiaweb_id: 1975
tax_id: 8453

layout: species
---

### Transcripts 
* Mean tx length(bp): 1,523
* Median tx length(bp): 1,146
* Tx longer than 400bp: 16,406 (80.718)
* Tx longer than 1kbp: 11,099 (54.608)
* Tx longer than 5kbp: 488
* Tx longer than 10kbp: 9

###  Peptides
* Mean peptide length(aa): 331
* Median peptide length(aa): 246
* Peptides longer than 100aa: 17,083 (84.049)
* Peptides longer than 500aa: 4,045 (19.902)
* Peptides longer than 1000aa: 675
* Peptides longer than 3000aa: 3

### BUSCO
* Vertebrates: C:65.3%[S:62.7%,D:2.6%],F:16.2%,M:18.5%,n:3354
  * 2192    Complete BUSCOs (C)
  * 2104    Complete and single-copy BUSCOs (S)
  * 88      Complete and duplicated BUSCOs (D)
  * 542     Fragmented BUSCOs (F)
  * 620     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
