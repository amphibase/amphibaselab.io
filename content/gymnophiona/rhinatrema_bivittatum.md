---
title: "Rhinatrema_bivittatum"
subtitle: "Two Banded Caecilia"
dirName: "Rhinatrema_bivittatum"

order: "Gymnophiona"
family: "Rhinatrematidae"

version: "2021_07"
seqs: 24,143
busco_pct: 91.6

amphibiaweb_id: 
tax_id: 194408

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,076
* Median Tx length(bp): 1,602
* Tx longer than 400bp: 20,944 (86.750)
* Tx longer than 1kbp: 16,086 (66.628)
* Tx longer than 5kbp: 1,719
* Tx longer than 10kbp: 121

###  Peptides
* Mean pep length(aa): 423
* Median pep length(aa): 308
* Peptides longer than 100aa: 21,005 (87.002)
* Peptides longer than 500aa: 6,800 (28.166)
* Peptides longer than 1000aa: 1,793
* Peptides longer than 3000aa: 55

### BUSCO
* Vertebrates: C:91.6%[S:88.3%,D:3.3%],F:3.5%,M:4.9%,n:3354
  * 3072    Complete BUSCOs (C)
  * 2961    Complete and single-copy BUSCOs (S)
  * 111     Complete and duplicated BUSCOs (D)
  * 119     Fragmented BUSCOs (F)
  * 163     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
