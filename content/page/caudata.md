---
title: Caudata
subtitle: AmphiBase page for salamanders
comments: false
---

## Repository

https://pub.amphibase.org/draft/Caudata/

## Species available on AmphiBase

### Ambystomatidae (32)

 - Ambystoma laterale
 - Ambystoma maculatum
 - Ambystoma mexicanum
 - Ambystoma texanum
 - Ambystoma tigrinum


### Cryptobranchidae (4)

 - Andrias japonicus


### Hynobiidae (86)

 - Batrachuperus yenyuanensis
 - Hynobius chinensis 
 - Hynobius leechii
 - Hynobius quelpaertensis
 - Hynobius retardatus
 - Hynobius unisacculus
 - Hynobius yangi
 - Onychodactylus koreanus


### Plethodontidae (491)

 - Bolitoglossa vallecula
 - Karsenia koreana


### Salamandridae (126)

 - Notophthalmus viridescens
 - Paramesotriton hongkongensis
 - Pleurodeles waltl
 - Salamandra salamandra
 - Tylototriton wenxianensis
 - Taricha granulosa


## Families without available species (yet).
 - Amphiumidae (3)
 - Dicamptodontidae (4)
 - Proteidae (8)
 - Rhyacotritonidae (4)
 - Sirenidae (5)
