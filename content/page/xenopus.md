---
title: Xenopus
subtitle: AmphiBase page for Xenopus and other related species
comments: false
---

### Xenopus laevis

 - Jbrowser @ XenBase: http://www.xenbase.org/common/displayJBrowse.do?data=data/xl9_2
 - UCSC Genome Browser: http://genome.ucsc.edu/cgi-bin/hgTracks?db=xenLae2

### Xenopus tropicalis

 - JBrowser @ XenBase: http://www.xenbase.org/common/displayJBrowse.do?data=data/xt10_0
 - UCSC Genome Brwoser: http://genome.ucsc.edu/cgi-bin/hgTracks?db=xenTro9

### Other related species

 - Xenopus andrei
 - Hymenochirus boettgeri

