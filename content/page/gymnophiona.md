---
title: Gymnophiona 
subtitle: AmphiBase page for caecilians
comments: false
---
## Repository

https://pub.amphibase.org/draft/Gymnophiona/

## Species available in AmphiBase

### Caeciliidae (43)
 - [*Caecilia tentaculata*]({{<ref "/gymnophiona/caecilia_tentaculata">}} "Caecilia tentaculata") - 2021_07; 22,226 seqs; BUSCOve 90%

### Ichthyophiidae (57)

 - Ichthyophis bannanicus


### Rhinatrematidae (14)

 - Rhinatrema bivittatum


### Siphonopidae (28)

 - Microcaecilia dermatophaga
 - Microcaecilia unicolor


## Typhlonectidae (14)

 - Typhlonectes compressicauda


## Families without any genomic resources (yet)
 - Chikilidae (4)
 - Dermophiidae (14)
 - Herpelidae (10)
 - Indotyphlidae (24)
 - Scolecomorphidae (6)
