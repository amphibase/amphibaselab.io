---
title: Anura
subtitle: AmphiBase page for forgs and toads
layout: species
comments: false
---

## Repository

https://pub.amphibase.org/draft/Anura/ 

## Species available on AmphiBase
 
### Bombinatoridae (10)
 - *Bombina bombina*
 - *Bombina orientalis*
 - *Bombina variegata*

### Bufonidae (637)
 - *Anaxyrus baxteri*
 - *Rhinella arenarum*
 - *Rhinella diptycha*
 - *Rhinella marina*
 - *Bufotes viridis*

### Dendrobatidae (329)
 - *Oophaga pumilio*

### Dicroglossidae (219)
 - *Nanorana parkeri*

### Hylidae (1022)
 - Boana pugnax
 - Hyla arborea
 - Hyla cinerea
 - [*Hyla suweonensis*]({{< ref "/anura/hyla_suweonensis">}} "Hyla suweonensis") - 2021_07; 22,929 seqs; BUSCOve 93%
 - Pseudacris regilla

### Leptodactylidae (224)
 - *Engystomops pustulosus*

### Mantellidae (232)
 - *Mantella betsileo*

### Megophryidae (273)
 - Leptobrachium boringii
 - Megophrys sangzhiensis

### Microhylidae (696)
 - [*Kaloula borealis*]({{< ref "/anura/kaloula_borealis">}} "Kaloula borealis") - 2021_07; 18,160 seqs; BUSCOve 80%
 - Microhyla fissipes

### Pelobatidae (6)
 - Pelobates cultripes

### Pipidae (41)
 - *Hymenochirus boettgeri*
 - *Xenopus andrei*
 - *Xenopus laevis*
 - *Xenopus tropicalis*

### Pyxicephalidae (87)
 - *Pyxicephalus adspersus*

### Ranidae (428)
 - *Odorrana margaretae*
 - *Odorrana tormota*
 - *Pelophylax chosenicus*
 - *Pelophylax lessonae*
 - *Pelophylax nigromaculatus*
 - *Rana catesbeianus*
 - *Rana clamitans*
 - *Rana pipiens*
 - *Rana sylvatica*
 - *Rana temporaria*
 - *Rana yavapaiensis*

### Rhacophoridae (439)
 - *Rhacophorus dennysi*
 - *Polypedates megacephalus*
 - *Zhangixalus omeimontis*

### Scaphiopodidae (7)
 - Scaphiopus couchii

### Strabomantidae (745)
 - Oreobates cruralis

----

## Families without genomic resources (yet).
 - Allophrynidae (3)
 - Alsodidae (26)
 - Alytidae (12)
 - Arthroleptidae (151)
 - Ascaphidae (2)
 - Batrachylidae (13)
 - Brachycephalidae (74)
 - Brevicipitidae (36)
 - Calyptocephalellidae (5)
 - Centrolenidae (160)
 - Ceratobatrachidae (102)
 - Ceratophryidae (12)
 - Ceuthomantidae (4)
 - Conrauidae (6)
 - Craugastoridae (123)
 - Cycloramphidae (36)
 - Eleutherodactylidae (229)
 - Heleophrynidae (6)
 - Hemiphractidae (118)
 - Hemisotidae (9)
 - Hylodidae (47)
 - Hyperoliidae (232)
 - Leiopelmatidae (3)
 - Micrixalidae (24)
 - Myobatrachidae (133)
 - Nasikabatrachidae (2)
 - Nyctibatrachidae (39)
 - Odontobatrachidae (5)
 - Odontophrynidae (50)
 - Pelodytidae (4)
 - Petropedetidae (13)
 - Phrynobatrachidae (97)
 - Ptychadenidae (63)
 - Ranixalidae (19)
 - Rhinodermatidae (3)
 - Rhinophrynidae (1)
 - Sooglossidae (4)
 - Telmatobiidae (63)
