---
title: About AmphiBase
subtitle: Genomic resources for frogs, toads, salamanders, and caecilians. 
comments: false
---

### Annotation Policy

To be announced. 

### Official Release

Currently the first official release is prepared targeting for the 2021 summer. 

### Up-to-date version

You can find the sequence information at https://pub.amphibase.org

### Contact

 - Taejoon Kwon (UNIST; Ulsan National Institute of Science and Technology) tkwon@unist.ac.kr
