---
title: Pleurodeles waltl
subtitle: Sharp-ribbed salamander 
dirName: Pleurodeles_waltl

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 25,735
busco_pct: 95.5

amphibiaweb_id: 4278
tax_id: 8319

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,841
* Median Tx length(bp): 2,161
* Tx longer than 400bp: 22,129 (85.988)
* Tx longer than 1kbp: 18,065 (70.196)
* Tx longer than 5kbp: 4,338
* Tx longer than 10kbp: 570


###  Peptides
* Mean peptide length(aa): 463
* Median peptide length(aa): 326
* Peptides longer than 100aa: 22,369 (86.921)
* Peptides longer than 500aa: 8,036 (31.226)
* Peptides longer than 1000aa: 2,437
* Peptides longer than 3000aa: 136


### BUSCO
* Vertebrates: C:95.5%[S:92.4%,D:3.1%],F:2.2%,M:2.3%,n:3354
  * 3202        Complete BUSCOs (C)
  * 3099        Complete and single-copy BUSCOs (S)
  * 103 Complete and duplicated BUSCOs (D)
  * 74  Fragmented BUSCOs (F)
  * 78  Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched
