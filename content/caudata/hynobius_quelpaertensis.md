---
title: Hynobius quelpaertensis
subtitle: Cheju Salamander
dirName: Hynobius_quelpaertensis

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 16,598
busco_pct: 63.7

amphibiaweb_id: 
tax_id: 113393

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,634
* Median Tx length(bp): 1,159
* Tx longer than 400bp: 14,685 (88.475)
* Tx longer than 1kbp: 9,143 (55.085)
* Tx longer than 5kbp: 569
* Tx longer than 10kbp: 30


###  Peptides
* Mean peptide length(aa): 348
* Median peptide length(aa): 255
* Peptides longer than 100aa: 15,700 (94.590)
* Peptides longer than 500aa: 3,259 (19.635)
* Peptides longer than 1000aa: 588
* Peptides longer than 3000aa: 11


### BUSCO
* Vertebrates: C:63.7%[S:61.2%,D:2.5%],F:14.1%,M:22.2%,n:3354
  * 2137        Complete BUSCOs (C)
  * 2052        Complete and single-copy BUSCOs (S)
  * 85  Complete and duplicated BUSCOs (D)
  * 473 Fragmented BUSCOs (F)
  * 744 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched
