---
title: Hynobius retardatus
subtitle: Hokkaido Salamander
dirName: Hynobius_retardatus

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 27,330
busco_pct: 78.9

amphibiaweb_id: 3893 
tax_id: 36312

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,504
* Median Tx length(bp): 961
* Tx longer than 400bp: 19,924 (72.902)
* Tx longer than 1kbp: 13,338 (48.804)
* Tx longer than 5kbp: 1,127
* Tx longer than 10kbp: 93


###  Peptides
* Mean peptide length(aa): 330
* Median peptide length(aa): 219
* Peptides longer than 100aa: 20,899 (76.469)
* Peptides longer than 500aa: 5,325 (19.484)
* Peptides longer than 1000aa: 1,226
* Peptides longer than 3000aa: 49

### BUSCO
* Vertebrates: C:78.9%[S:75.4%,D:3.5%],F:9.2%,M:11.9%,n:3354
  * 2646 	Complete BUSCOs (C)
  * 2529  Complete and single-copy BUSCOs (S)
  * 117	Complete and duplicated BUSCOs (D)
  * 307 Fragmented BUSCOs (F)
  * 401 Missing BUSCOs (M)
  * 3354	Total BUSCO groups searched

