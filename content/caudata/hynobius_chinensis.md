---
title: Hynobius chinensis
subtitle: Chinese Salamander
dirName: Hynobius_chinensis

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 23,408
busco_pct: 56.7

amphibiaweb_id: 3881
tax_id: 288313

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,086
* Median Tx length(bp): 729
* Tx longer than 400bp: 16,142 (68.959)
* Tx longer than 1kbp: 9,181 (39.222)
* Tx longer than 5kbp: 230
* Tx longer than 10kbp: 16

###  Peptides
* Mean peptide length(aa): 279
* Median peptide length(aa): 195
* Peptides longer than 100aa: 17,449 (74.543)
* Peptides longer than 500aa: 3,343 (14.281)
* Peptides longer than 1000aa: 539
* Peptides longer than 3000aa: 12


### BUSCO
* Vertebrates: C:56.7%[S:54.3%,D:2.4%],F:22.6%,M:20.7%,n:3354
  * 1901    Complete BUSCOs (C)
  * 1820    Complete and single-copy BUSCOs (S)
  * 81      Complete and duplicated BUSCOs (D)
  * 757     Fragmented BUSCOs (F)
  * 696     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
