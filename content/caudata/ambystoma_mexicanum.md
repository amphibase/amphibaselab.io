---
title: Ambystoma mexicanum 
subtitle: Mexican Axolotl
dirName: Ambystoma_mexicanum

order: Caudata
family:  Ambystomatidae

version: "2021_07"
seqs: 27,889
busco_pct: 95.7

amphibiaweb_id: 3842 
tax_id: 8296

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,623
* Median Tx length(bp): 2,057
* Tx longer than 400bp: 24,248 (86.914)
* Tx longer than 1kbp: 19,420 (69.608)
* Tx longer than 5kbp: 3,810
* Tx longer than 10kbp: 379

###  Peptides
* Mean peptide length(aa): 452
* Median peptide length(aa): 322
* Peptides longer than 100aa: 24,706 (88.555)
* Peptides longer than 500aa: 8,482 (30.403)
* Peptides longer than 1000aa: 2,474
* Peptides longer than 3000aa: 103

### BUSCO
* Vertebrates: C:95.7%[S:92.4%,D:3.3%],F:1.8%,M:2.5%,n:3354
  * 3209        Complete BUSCOs (C)
  * 3099        Complete and single-copy BUSCOs (S)
  * 110 Complete and duplicated BUSCOs (D)
  * 62  Fragmented BUSCOs (F)
  * 83  Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

