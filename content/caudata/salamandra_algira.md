---
title: Salamandra algira
subtitle: Algerian Salamander
dirName: Salamandra_algira

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 8592
busco_pct: 35.2 

amphibiaweb_id: 4280 
tax_id: 220703

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,491
* Median Tx length(bp): 1,207
* Tx longer than 400bp: 7,504 (87.337)
* Tx longer than 1kbp: 5,049 (58.764)
* Tx longer than 5kbp: 132
* Tx longer than 10kbp: 4


###  Peptides
* Mean peptide length(aa): 303
* Median peptide length(aa): 244
* Peptides longer than 100aa: 7,545 (87.814)
* Peptides longer than 500aa: 1,236 (14.385)
* Peptides longer than 1000aa: 140
* Peptides longer than 3000aa: 0


### BUSCO
* Vertebrates: C:35.2%[S:34.1%,D:1.1%],F:13.0%,M:51.8%,n:3354
  * 1183        Complete BUSCOs (C)
  * 1145        Complete and single-copy BUSCOs (S)
  * 38  Complete and duplicated BUSCOs (D)
  * 437 Fragmented BUSCOs (F)
  * 1734        Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched
