---
title: Hynobius leechii
subtitle: Korean Salamander
dirName: Hynobius_leechii

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 18,272
busco_pct: 71.2

amphibiaweb_id: 3886 
tax_id: 113391

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,844
* Median Tx length(bp): 1,353
* Tx longer than 400bp: 16,427 (89.903)
* Tx longer than 1kbp: 11,099 (60.743)
* Tx longer than 5kbp: 964
* Tx longer than 10kbp: 46

###  Peptides
* Mean peptide length(aa): 384
* Median peptide length(aa): 287
* Peptides longer than 100aa: 17,457 (95.540)
* Peptides longer than 500aa: 4,373 (23.933)
* Peptides longer than 1000aa: 897
* Peptides longer than 3000aa: 13

### BUSCO
* Vertebrates: C:71.2%[S:68.2%,D:3.0%],F:11.7%,M:17.1%,n:3354
  * 2391        Complete BUSCOs (C)
  * 2289        Complete and single-copy BUSCOs (S)
  * 102 Complete and duplicated BUSCOs (D)
  * 392 Fragmented BUSCOs (F)
  * 571 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

