---
title: Onychodactylus koreanus
subtitle: Korean Clawed Salamander
dirName: Onychodactylus_koreanus

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 20,758
busco_pct: 69.1

amphibiaweb_id: 7899  
tax_id: 1248180

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,552
* Median Tx length(bp): 1,128
* Tx longer than 400bp: 16,609 (80.013)
* Tx longer than 1kbp: 11,223 (54.066)
* Tx longer than 5kbp: 724
* Tx longer than 10kbp: 38


###  Peptides
* Mean peptide length(aa): 342
* Median peptide length(aa): 251
* Peptides longer than 100aa: 17,248 (83.091)
* Peptides longer than 500aa: 4,176 (20.118)
* Peptides longer than 1000aa: 800
* Peptides longer than 3000aa: 18

### BUSCO
* Vertebrates: C:69.1%[S:66.2%,D:2.9%],F:10.9%,M:20.0%,n:3354
  * 2315        Complete BUSCOs (C)
  * 2219        Complete and single-copy BUSCOs (S)
  * 96  Complete and duplicated BUSCOs (D)
  * 365 Fragmented BUSCOs (F)
  * 674 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

