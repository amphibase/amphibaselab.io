---
title: Karsenia koreana
subtitle: Korean Crevice Salamander
dirName: Karsenia_koreana

order: Caudata
family: Plethodontidae

version: "2021_07"
seqs: 20,836
busco_pct: 82.8

amphibiaweb_id: 6429
tax_id: 322013

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,926
* Median Tx length(bp): 1,494
* Tx longer than 400bp: 18,184 (87.272)
* Tx longer than 1kbp: 13,176 (63.237)
* Tx longer than 5kbp: 1,139
* Tx longer than 10kbp: 30

###  Peptides
* Mean peptide length(aa): 385
* Median peptide length(aa): 288
* Peptides longer than 100aa: 18,485 (88.717)
* Peptides longer than 500aa: 5,288 (25.379)
* Peptides longer than 1000aa: 1,154
* Peptides longer than 3000aa: 7

### BUSCO
* Vertebrates: C:82.8%[S:79.4%,D:3.4%],F:7.0%,M:10.2%,n:3354
  * 2777        Complete BUSCOs (C)
  * 2663        Complete and single-copy BUSCOs (S)
  * 114 Complete and duplicated BUSCOs (D)
  * 234 Fragmented BUSCOs (F)
  * 343 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

