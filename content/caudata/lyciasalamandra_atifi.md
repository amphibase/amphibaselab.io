---
title: Lyciasalamandra atifi
subtitle: 
dirName: Lyciasalamandra_atifi 

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 10,149
busco_pct: 39.7

amphibiaweb_id: 
tax_id: 297010

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,686
* Median Tx length(bp): 1,270
* Tx longer than 400bp: 8,799 (86.698)
* Tx longer than 1kbp: 6,088 (59.986)
* Tx longer than 5kbp: 398
* Tx longer than 10kbp: 21


###  Peptides
* Mean peptide length(aa): 343
* Median peptide length(aa): 263
* Peptides longer than 100aa: 8,935 (88.038)
* Peptides longer than 500aa: 1,952 (19.233)
* Peptides longer than 1000aa: 339
* Peptides longer than 3000aa: 5


### BUSCO
* Vertebrates: C:39.7%[S:37.9%,D:1.8%],F:14.0%,M:46.3%,n:3354
  * 1331        Complete BUSCOs (C)
  * 1271        Complete and single-copy BUSCOs (S)
  * 60  Complete and duplicated BUSCOs (D)
  * 470 Fragmented BUSCOs (F)
  * 1553        Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

