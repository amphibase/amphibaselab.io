---
title: Caudata species at AmphiBase
date: 2021-08-08
layout: order_list

AvailableFamilies: [
  "Ambystomatidae (32)", 
  "Cryptobranchidae (4)", 
  "Hynobiidae (86)", 
  "Plethodontidae (491)", 
  "Salamandridae (126)"
]

NotAvailableFamilies: [
  "Amphiumidae (3)", 
  "Dicamptodontidae (4)", 
  "Proteidae (8)", 
  "Rhyacotritonidae (4)", 
  "Sirenidae (5)"
]

---

**Repository:** https://pub.amphibase.org/draft/Caudata/
