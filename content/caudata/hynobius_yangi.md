---
title: Hynobius yangi
subtitle: Kori salamander
dirName: Hynobius_yangi

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 20,287
busco_pct: 73.8

amphibiaweb_id: 6217 
tax_id: 586894

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,770
* Median Tx length(bp): 1,336
* Tx longer than 400bp: 18,132 (89.377)
* Tx longer than 1kbp: 12,241 (60.339)
* Tx longer than 5kbp: 840
* Tx longer than 10kbp: 51

###  Peptides
* Mean peptide length(aa): 379
* Median peptide length(aa): 282
* Peptides longer than 100aa: 19,401 (95.633)
* Peptides longer than 500aa: 4,632 (22.832)
* Peptides longer than 1000aa: 959
* Peptides longer than 3000aa: 28


### BUSCO
* Vertebrates: C:73.8%[S:71.0%,D:2.8%],F:10.3%,M:15.9%,n:3354
  * 2476    Complete BUSCOs (C)
  * 2383    Complete and single-copy BUSCOs (S)
  * 93      Complete and duplicated BUSCOs (D)
  * 345     Fragmented BUSCOs (F)
  * 533     Missing BUSCOs (M)
  * 3354    Total BUSCO groups searched
