---
title: Lyciasalamandra billae
subtitle: Bay Lycian Salamander
dirName: Lyciasalamandra_billae

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 9306
busco_pct: 35.8 

amphibiaweb_id: 6349
tax_id: 156866

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,673
* Median Tx length(bp): 1,281
* Tx longer than 400bp: 8,221 (88.341)
* Tx longer than 1kbp: 5,629 (60.488)
* Tx longer than 5kbp: 342
* Tx longer than 10kbp: 13


###  Peptides
* Mean peptide length(aa): 343
* Median peptide length(aa): 265
* Peptides longer than 100aa: 8,349 (89.716)
* Peptides longer than 500aa: 1,745 (18.751)
* Peptides longer than 1000aa: 311
* Peptides longer than 3000aa: 4

### BUSCO
* Vertebrates: C:35.8%[S:34.5%,D:1.3%],F:13.9%,M:50.3%,n:3354
  * 1201        Complete BUSCOs (C)
  * 1156        Complete and single-copy BUSCOs (S)
  * 45  Complete and duplicated BUSCOs (D)
  * 465 Fragmented BUSCOs (F)
  * 1688        Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

