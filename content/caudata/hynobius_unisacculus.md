---
title: Hynobius unisacculus
subtitle: 
dirName: Hynobius_unisacculus

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 26,847
busco_pct: 73.9

amphibiaweb_id: 
tax_id: 2606758

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,521
* Median Tx length(bp): 1,021
* Tx longer than 400bp: 21,672 (80.724)
* Tx longer than 1kbp: 13,638 (50.799)
* Tx longer than 5kbp: 986
* Tx longer than 10kbp: 51

###  Peptides
* Mean peptide length(aa): 339
* Median peptide length(aa): 241
* Peptides longer than 100aa: 23,055 (85.876)
* Peptides longer than 500aa: 5,252 (19.563)
* Peptides longer than 1000aa: 1,092
* Peptides longer than 3000aa: 18

### BUSCO
* Vertebrates: C:73.9%[S:71.3%,D:2.6%],F:11.4%,M:14.7%,n:3354
  * 2478	Complete BUSCOs (C)
  * 2391	Complete and single-copy BUSCOs (S)
  * 87  Complete and duplicated BUSCOs (D)
  * 382 Fragmented BUSCOs (F)
  * 494 Missing BUSCOs (M)
  * 3354	Total BUSCO groups searched
