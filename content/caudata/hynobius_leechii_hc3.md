---
title: Hynobius leechii (HC3)
subtitle: Korean Salamander
dirName: Hynobius_leechii-HC3

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 15,747
busco_pct: 60.8

amphibiaweb_id: 3886 
tax_id: 113391

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,545
* Median Tx length(bp): 1,184
* Tx longer than 400bp: 14,281 (90.690)
* Tx longer than 1kbp: 9,001 (57.160)
* Tx longer than 5kbp: 360
* Tx longer than 10kbp: 29


###  Peptides
* Mean peptide length(aa): 353
* Median peptide length(aa): 267
* Peptides longer than 100aa: 14,933 (94.831)
* Peptides longer than 500aa: 3,103 (19.705)
* Peptides longer than 1000aa: 519
* Peptides longer than 3000aa: 14


### BUSCO
* Vertebrates: C:60.8%[S:58.5%,D:2.3%],F:16.4%,M:22.8%,n:3354
  * 2040        Complete BUSCOs (C)
  * 1963        Complete and single-copy BUSCOs (S)
  * 77  Complete and duplicated BUSCOs (D)
  * 551 Fragmented BUSCOs (F)
  * 763 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

