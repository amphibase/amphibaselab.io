---
title: Hynobius leechii (HC1)
subtitle: Korean Salamander
dirName: Hynobius_leechii-HC1

order: Caudata
family: Hynobiidae

version: "2021_07"
seqs: 23,292
busco_pct: 79.9

amphibiaweb_id: 3886 
tax_id: 113391

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,886
* Median Tx length(bp): 1,315
* Tx longer than 400bp: 20,752 (89.095)
* Tx longer than 1kbp: 13,776 (59.145)
* Tx longer than 5kbp: 1,466
* Tx longer than 10kbp: 107


###  Peptides
* Mean peptide length(aa): 390
* Median peptide length(aa): 279
* Peptides longer than 100aa: 22,115 (94.947)
* Peptides longer than 500aa: 5,588 (23.991)
* Peptides longer than 1000aa: 1,315
* Peptides longer than 3000aa: 39


### BUSCO
* Vertebrates: C:79.9%[S:76.8%,D:3.1%],F:8.4%,M:11.7%,n:3354
  * 2681        Complete BUSCOs (C)
  * 2577        Complete and single-copy BUSCOs (S)
  * 104 Complete and duplicated BUSCOs (D)
  * 282 Fragmented BUSCOs (F)
  * 391 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

