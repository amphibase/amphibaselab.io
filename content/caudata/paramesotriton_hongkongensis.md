---
title: Paramesotriton hongkongensis
subtitle: Hong Kong Warty New
dirName: Paramesotriton_hongkongensis

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 20,494
busco_pct: 63.9

amphibiaweb_id: 5918
tax_id: 164972

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,601
* Median Tx length(bp): 1,124
* Tx longer than 400bp: 16,120 (78.657)
* Tx longer than 1kbp: 11,020 (53.772)
* Tx longer than 5kbp: 782
* Tx longer than 10kbp: 13


###  Peptides
* Mean peptide length(aa): 330
* Median peptide length(aa): 241
* Peptides longer than 100aa: 16,974 (82.824)
* Peptides longer than 500aa: 3,993 (19.484)
* Peptides longer than 1000aa: 785
* Peptides longer than 3000aa: 1


### BUSCO
* Vertebrates: C:63.9%[S:61.0%,D:2.9%],F:16.2%,M:19.9%,n:3354
  * 2142        Complete BUSCOs (C)
  * 2045        Complete and single-copy BUSCOs (S)
  * 97  Complete and duplicated BUSCOs (D)
  * 544 Fragmented BUSCOs (F)
  * 668 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

