---
title: Andrias davidianus 
subtitle: Chinese Giant Salamander
dirName: Andrias_davidianus 

order: Caudata
family: Cryptobranchidae

version: "2021_07"
seqs: 16l662
busco_pct: 87.9

amphibiaweb_id: 3858 
tax_id: 141262

layout: species
---

### Transcripts 
* Mean Tx length(bp): 2,865
* Median Tx length(bp): 2,378
* Tx longer than 400bp: 16,165 (97.017)
* Tx longer than 1kbp: 14,338 (86.052)
* Tx longer than 5kbp: 2,035
* Tx longer than 10kbp: 169


###  Peptides
* Mean peptide length(aa): 543
* Median peptide length(aa): 420
* Peptides longer than 100aa: 16,140 (96.867)
* Peptides longer than 500aa: 6,669 (40.025)
* Peptides longer than 1000aa: 1,907
* Peptides longer than 3000aa: 74

### BUSCO
* Vertebrates: C:87.9%[S:86.7%,D:1.2%],F:3.2%,M:8.9%,n:3354
  * 2948        Complete BUSCOs (C)
  * 2909        Complete and single-copy BUSCOs (S)
  * 39  Complete and duplicated BUSCOs (D)
  * 107 Fragmented BUSCOs (F)
  * 299 Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

