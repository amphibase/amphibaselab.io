---
title: Salamandra atra
subtitle: Alpine salamander
dirName: Salamandra_atra

order: Caudata
family: Salamandridae

version: "2021_07"
seqs: 10,122
busco_pct: 42.4

amphibiaweb_id: 4281
tax_id: 57570

layout: species
---

### Transcripts 
* Mean Tx length(bp): 1,703
* Median Tx length(bp): 1,327
* Tx longer than 400bp: 8,963 (88.550)
* Tx longer than 1kbp: 6,294 (62.181)
* Tx longer than 5kbp: 340
* Tx longer than 10kbp: 20

###  Peptides
* Mean peptide length(aa): 345
* Median peptide length(aa): 271
* Peptides longer than 100aa: 9,082 (89.725)
* Peptides longer than 500aa: 1,947 (19.235)
* Peptides longer than 1000aa: 308
* Peptides longer than 3000aa: 3


### BUSCO
* Vertebrates: C:42.4%[S:41.1%,D:1.3%],F:13.2%,M:44.4%,n:3354
  * 1424        Complete BUSCOs (C)
  * 1380        Complete and single-copy BUSCOs (S)
  * 44  Complete and duplicated BUSCOs (D)
  * 444 Fragmented BUSCOs (F)
  * 1486        Missing BUSCOs (M)
  * 3354        Total BUSCO groups searched

