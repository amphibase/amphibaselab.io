## About

AmphiBase is the hub for amphibian species genomic resources
(frogs, toads, newt, salamanders, and caecilians). 
It starts to collect protein-coding genes from transcriptome data 
(mainly using de novo transcriptome assembly) and plan to expand it for genome sequences. 


## Other sites related to amphibian genomic resources

* XenBase: a central information hub for Xenopus genomes https://www.xenbase.org 
* AmphibiaWeb: a knowledgebase for amphibian species https://amphibiaweb.org/
* SalSite


## Funding

AmphiBase was supported by a grant from the National Institute of Biological Resources (NIBR), 
funded by the Ministry of Environment (MOE) of the Republic of Korea 
(NIBR201803101; NIBR201905103; NIBR202005102).

## Contact

If you have any questions or inquiries, please contact Taejoon Kwon (tkwon@unist.ac.kr).
